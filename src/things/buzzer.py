# Python code, triggers the buzzer
import time
import RPi.GPIO as gpio
import sys

pin = int(sys.argv[1]) # for example 22
value = int(sys.argv[2]) # boolean
gpio.setwarnings(False)
gpio.setmode(gpio.BOARD)
gpio.setup(pin, gpio.OUT)

try:
    gpio.output(pin, value)
    print('{"httpCode": "200", "type": "SUCCESS", "message": "All good.", "data": []}')
except Exception as e:
    print('{"httpCode": "400", "type": "ERROR", "message": "Something hit the fan in buzzer.py: '+str(e)+', "data": []}')
    gpio.cleanup()
exit