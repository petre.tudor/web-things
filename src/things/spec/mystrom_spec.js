const { MyStrom } = require('../my-strom')

describe('MyStrom', () => {
	var myStrom
	beforeAll(() => {
		myStrom = new MyStrom()
	})
	
	it('On/Off switch', () => {
		var request = jasmine.createSpy('reqest')
		myStrom.request = request
		myStrom.thing_name = 'mockThingName'
		let value = 0
		let url = 'mockUrl:3001'
		myStrom.onOff(value, url)
		expect(request).toHaveBeenCalledWith(url + '?state=' + value, jasmine.anything())
		
		value = 1
		url = 'mockUrl:3012'
		myStrom.onOff(value, url)
		expect(request).toHaveBeenCalledWith(url + '?state=' + value, jasmine.anything())
		
		//console.log('2:', buzzer)
	})
})
