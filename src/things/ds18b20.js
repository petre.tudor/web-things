const {
  Property,
  Thing,
  Value,
  Action,
} = require('webthing')
const ds18b20 = require('ds18b20-raspi')
const log = require('./print')

class RefreshAction extends Action {
  constructor(thing, input) {
    log(input)
    super(44, thing, 'refresh', input);
  }
  //Is reading the temperature when you press the action button!
  performAction() {
    return new Promise((resolve) => {
      log("Manual Temperature refresh")
      let newLevel = ds18b20.readSimpleC()
      this.thing.level.notifyOfExternalUpdate(newLevel.toFixed(2))
    })
  }
}
/**
 * A temperature sensor which updates its measurement every 5 minutes.
 */
class DS18B20Sensor extends Thing {
  constructor() {
    super('ds18b20 Temperature Sensor',
      ['MultiLevelSensor'],
      'A web connected temperature sensor')
    // Creating the temperature property
    this.level = new Value(1.0)
    this.addProperty(
      new Property(
        this,
        'level',
        this.level,
        {
          '@type': 'LevelProperty',
          label: 'Temperature',
          type: 'number',
          description: 'The current temperature in Celsius',
          minimum: -10,
          maximum: 50,
          unit: 'celsius',
        }))

    // Poll the sensor reading every 5 min
    setInterval(() => {
      // Update the underlying value, which in turn notifies all listeners
      let newLevel = ds18b20.readSimpleC()
      this.level.notifyOfExternalUpdate(newLevel.toFixed(2))
      log('Automatic temperature level updated: ' + newLevel.toFixed(2))
    }, 300000)
    // Manual refresh for temperature
    this.addAvailableAction(
      'refresh',
      {
        label: 'refresh',
        description: 'Refresh the temperature'
      },
      RefreshAction)
  }
}
module.exports = { DS18B20Sensor }
