import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
LED = 21
GPIO.setup(LED,GPIO.OUT)
GPIO.output(LED,True)
time.sleep(5)
GPIO.output(LED,False)