#! /bin/bash

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   echo "please run: sudo $(readlink -f $0)"
   exit 1
fi


sed -i '/WEB_THINGS_ROOT/d' /etc/environment
echo "WEB_THINGS_ROOT=/webThings" >> /etc/environment
. /etc/environment

/webThings/scripts/createUser.sh
/webThings/scripts/patch.sh
