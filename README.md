This is a small project for running [web-things](https://iot.mozilla.org/wot/) in a [nodejs server](https://github.com/mozilla-iot/webthing-node/) on a rasperry pi.
It has a developent enviroment using vagrant and install/update scripts.


## development enviroment install(with vagrant)

- install [vagrant](https://www.vagrantup.com/) on your machine
- clone this project: `git clone https://gitlab.com/petre.tudor/web-things`
- enter folder and start the vagrant vm: `cd webThings && vagrant up --provider=virtualbox`
- ssh into vm: `vagrant ssh`
- `cd /webThings/scripts && sudo ./initialSetup.sh`
- watch the magic happen (and enjoy some music https://www.youtube.com/watch?v=qtZJiQSmJ9g)
- swich user: `sudo su gradinar`
- see actions running by watching the logs: `pm2 logs`
- `web things` can now be discovered from things gateway by adding: [ip]:3003 in addon settings


### not setup yet
- grafana: http://localhost:3002 (admin/admin)
    - adding a source
        - Type: InfluxDB
        - URL: http://localhost:8086
        - Access: proxy
    - add dashboards...
    - add users....



## getting updates

- sudo su gradinar
- cd /piGarden/
- git checkout master
- git pull
- sudo ./scripts/patch.sh
